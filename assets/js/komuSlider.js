// Can also be used with $(document).ready()
$(window).load(function() {
  $(sliderClass).flexslider({
    animation: "slide",
    controlNav : true,
    slideshow : true,
  });
});