<?php

/**
 * komuFlexSlider
 * =================
 * This extension adjusts the pictures to specified width and height without stretch or changing picture's ratio.
 * So there is no need to resize your image or worry about the resolution of user's screen
 * The extension uses javascript functions to show a centered part for each picture.
 * Links:
 * - 
 * - komuFlexSlider site: http://www.yiiframework.com/extension/TODO
 *
 * @version 1.0
 * @author John Komu<komu.john@gmail.com>
 * @date Sept 2013
 * */
class komuFlexSlider extends CWidget {

    private $assets;
    public $sliderClass = '#komuslider';

    public function init() {
        $this->assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');
        Yii::app()->getClientScript()->registerScriptFile($this->assets . '/js/jquery.flexslider.js');
		Yii::app()->getClientScript()->registerScriptFile($this->assets . '/js/komuSlider.js');
		Yii::app()->getClientScript()->registerCssFile($this->assets . '/css/flexslider.css');
		//Yii::app()->getClientScript()->registerCssFile($this->assets . '/css/demo.css');
    }

    public function run() {

        Yii::app()->getClientScript()->registerScript('slider.variables', "
             var sliderClass='$this->sliderClass';
             ", CClientScript::POS_BEGIN);
    }

}

?>
